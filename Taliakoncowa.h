
#include <iostream>
#include <windows.h>
#include <ctime>
#include <stdlib.h>
#include <conio.h>
using namespace std;

struct karta{
	int kolor;
	int numer;
	bool czyOdkryta;
};

struct taliaKarta
{
	karta card;
	taliaKarta *next;
	taliaKarta *prev;
};

class talia
{
public:
	//void InitList(int col, int wart);
	//void Wstaw(int nr, int col, int wart);
	//void DopiszNaKoniec(int col, int wart);
	//void DopiszNaPoczatek(int col, int wart);
	void Dopisz(taliaKarta *wsk);
	void DopiszKarte(karta *wsk);
	taliaKarta* Wydaj(int index);
	//LIST_ELEM* Wypisz(int index);
	void Tasuj();
	void WypiszListe();
	taliaKarta* WydajOstat();
	//void UsunListe();
	void StworzTalie();
	void VisualDisp();
	void Delete();
	taliaKarta* GetTail() { return tail; }
	taliaKarta* GetHead() { return head; }

private:
	taliaKarta *head;
	taliaKarta *tail;
};

void GoToXY(int x, int y);

void WyswietlKarteXY(int numer, int symbol, int col, int row);


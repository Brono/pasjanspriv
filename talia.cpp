//Autor: Jakub Bronowski
//Wszyskie prawa zastrzerzone.
//Kopjowanie lub publiczne wykorzystywanie surowo
//wzbronione pod gro�bami prawa karnego.

#include <iostream>
#include <windows.h>
#include <ctime>
#include <stdlib.h>
#include <conio.h>
using namespace std;

//////////////////////////////////////
/////Struktura karta i taliaKarta/////
//////////////////////////////////////

typedef struct karta{
    int kolor;
    int numer;
};

struct taliaKarta
{
    karta card;
	taliaKarta *next;
	taliaKarta *prev;
};

//////////////////////////////////////
/////      Klasa talia     ///////////
//////////////////////////////////////

class talia
{
public:
    	talia();
	~talia();
	//void InitList(int col, int wart);
	//void Wstaw(int nr, int col, int wart);
	//void DopiszNaKoniec(int col, int wart);
	//void DopiszNaPoczatek(int col, int wart);
	void Dopisz(taliaKarta *wsk);
	void DopiszKarte(karta *wsk);
	taliaKarta* Wydaj(int index);
	//LIST_ELEM* Wypisz(int index);
	void Tasuj();
	void WypiszListe();
	taliaKarta* WydajOstat();
	//void UsunListe();
	void StworzTalie();
	void VisualDisp();

private:
	taliaKarta *head;
	taliaKarta *tail;
};

void talia::StworzTalie()
{
	taliaKarta *wsk;
	wsk = new taliaKarta;
	head = wsk;
	for (int col = 1; col <= 4; col++){
		for (int num = 1; num <= 13; num++){
			wsk->card.kolor = col;
			wsk->card.numer = num;
			if (num == 13 && col == 4){
				wsk->next = NULL;
			}else{
				wsk->next = new taliaKarta;
				wsk->next->prev = wsk;
				wsk = wsk->next;
			}

		}
	}
	head->prev = NULL;
	tail = wsk;
}

taliaKarta* talia::Wydaj(int index)
{
	taliaKarta * toGive;
	toGive = head;
	for (int i = 0; i < index; i++)
	{
		if (NULL != toGive->next)
			toGive = toGive->next;
		else
			break;
	}
	if (NULL != toGive->next)
		toGive->next->prev = toGive->prev;
	else
		tail = toGive->prev;

	if (NULL != toGive->prev)
		toGive->prev->next = toGive->next;
	else
		head = toGive->next;

	return toGive;
}

taliaKarta* talia::WydajOstat()
{
    taliaKarta * toGive;
    toGive = tail;
    tail->prev->next = NULL;
    tail = tail->prev;
}

void talia::Dopisz(taliaKarta *wsk)
{
	wsk->prev = tail;
	wsk->next = NULL;
	tail->next = wsk;
	tail = wsk;
}

void talia::DopiszKarte(karta *wsk)
{
    taliaKarta *newWsk = new taliaKarta;
    newWsk->card.kolor = wsk->kolor;
    newWsk->card.numer = wsk->numer;
    newWsk->prev = tail;
	newWsk->next = NULL;
	tail->next = newWsk;
	tail = newWsk;
}

void talia::WypiszListe()
{
	taliaKarta *toDisp;
	toDisp = head;
	while (NULL != toDisp)
	{
		cout << "Kolor karty: " << toDisp->card.kolor << "\t" << "Wartosc karty: " << toDisp->card.numer << endl;
		toDisp = toDisp->next;
	}
}

talia::talia()
{

}

talia::~talia()
{

}

void talia::VisualDisp()
{
    taliaKarta *wsk;
	wsk = head;
    int num = 0;
    while (NULL != wsk->next)
	{
		num++;
		wsk = wsk->next;
	};
	cout << "Kart w tali: " << num << " X. karta na szczycie: ";
    WysKart(tail->card.numer, tail->card.kolor, 7, 4, 0);
}

//////////////////////////////////////
/////    Funkcja GoToXY    ///////////
//////////////////////////////////////

void GoToXY(int x, int y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}

//////////////////////////////////////
/////    Funkcja WysKart   ///////////
//////////////////////////////////////

void WysKart(int numer, int symbol, int col, int row, int aka, int space = 1)// int space)
{
    row = row * 3;
    GoToXY(col*5+(space*col)+aka, row);
    cout << (char)218 << (char)196 << (char)196 << (char)196 << (char)191 << endl;
    row++;
    GoToXY(col*5+(space*col)+aka, row);
    cout << (char)179;
    switch(numer)
    {
    case 13:
        cout << "K ";
        break;
    case 12:
        cout << "Q ";
        break;
    case 11:
        cout << "J ";
        break;
    case 1:
        cout << "A ";
        break;
    case 2:
        cout << "2 ";
        break;
    case 3:
        cout << "3 ";
        break;
    case 4:
        cout << "4 ";
        break;
    case 5:
        cout << "5 ";
        break;
    case 6:
        cout << "6 ";
        break;
    case 7:
        cout << "7 ";
        break;
    case 8:
        cout << "8 ";
        break;
    case 9:
        cout << "9 ";
        break;
    case 10:
        cout << "10";
        break;
    default:
        cout << "##";
    }
    switch(symbol)
    {
    case 1:
        cout << (char)3;
        break;
    case 2:
        cout << (char)4;
        break;
    case 3:
        cout << (char)5;
        break;
    case 4:
        cout << (char)6;
        break;
    default:
        cout << "#";
        break;
    }
    cout << (char)179 << endl;
    row++;
    GoToXY(col*5+(space*col)+aka, row);
    cout << (char)192 << char(196) << (char)196 << (char)196 << (char)217 << endl;
}